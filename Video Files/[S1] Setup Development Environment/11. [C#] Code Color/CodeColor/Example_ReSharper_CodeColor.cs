﻿#region Plain Text

using System;
using System.Text.RegularExpressions;
using ExampleResharperCodeColor.MyOtherNamespace;

namespace ExampleResharperCodeColor
{
    internal class UserTypesClasses : IUserTypesInterfaces
    {
        public delegate void UserTypesDelegates();

        public const string CONSTANT_IDENTIFIER = "";
        private static int _fieldIdentifier = 123456789;

        /// <Xml_Doc_Comments_Name>
        ///     XML Doc Comment - Text
        ///     The quick brown fox jumps over the lazy dog
        ///     THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
        /// </Xml_Doc_Comments_Name>
        public static void MethodIdentifier(string[] args)
        {
            var identifierString = "The time now is approximately " + DateTime.Now;
            var identifierUri = new Uri("http://packmyboxwith/jugs.html?q=five-dozen&t=liquor");
            var identifierRegex = new Regex(@"\S#$", RegexOptions.IgnorePatternWhitespace);
            for (var O = 0; O < 123456789; O++)
            {
                _fieldIdentifier += O%3*((O/1) ^ 2) - 5;
                if (!identifierRegex.IsMatch(identifierUri.ToString()))
                {
                    // Comment
                    Console.WriteLine(identifierUri + identifierString + _fieldIdentifier);
                    var myEnum = MyEnum.ValueA;
                    MyStruct.ValueA = 10;
                    var myStruct = new MyStruct();
                    myStruct.PropertyA = 10;
                }
            }

            var myExtendedClass = new MyExtendedClass();
            myExtendedClass.ExtensionMethod();
        }
    }

    #endregion

    public class Highlighting
    {
        private string highlightedRef;
        private string inactiveSelection = "select this";
        /* Collapsed Text
        */
        private string selectString = "";

        public void Features()
        {
            highlightedRef = "again";
            var breakPoint = "breakpoint";
#if PREPROCESSOR
        object excludedCode = null;
#endif
            breakPoint = "current statement";
        }
    }

    public class MyClass<T> where T : class
    {
    }

    public enum MyEnum
    {
        ValueA
    }

    public struct MyStruct
    {
        public static object ValueA;

        public int PropertyA { get; set; }
    }

    namespace MyOtherNamespace
    {
        internal interface IUserTypesInterfaces
        {
        }
    }

    public static class MyStaticClass
    {
        public static void ExtensionMethod(this MyExtendedClass theExtendedClass)
        {
        }
    }

    public class MyExtendedClass
    {
    }

    public class DeadCode
    {
        private void DeadMethod()
        {
        }
    }
}