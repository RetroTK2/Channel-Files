﻿/* #region
    Text Editor → Preprocessor Keyword → Foreground
    #FFFF00EA
*/
/* Plain Text
    Text Editor → Plain Text → Foreground
    #FF000000
*/

#region Plain Text

/* using
    Text Editor → Keyword → Foreground
    #FF007AFF
*/
/* System
    Text Editor → Identifier → Foreground
    #FF953BA3
*/
/* ;
    Text Editor → Plain Text → Foreground
    #FF000000
*/
using System;
using System.Text.RegularExpressions;

/* class
    Text Editor → Keyword → Foreground
    #FF007AFF
*/
/* UserTypesClasses
    Text Editor → User Types - Classes → Foreground
    #FF53D327
*/
/* IUserTypesInterfaces
    Text Editor → User Types - Interfaces → Foreground
    #FF030772
*/
/* StandardCodeColor
    Text Editor → Identifier → Foreground
    #FF953BA3
*/

namespace StandardCodeColor
{
    class UserTypesClasses : IUserTypesInterfaces
    {
        /* _identifier
            Text Editor → Identifier → Foreground
            #FF953BA3
        */
        /* 123456789
            Text Editor → Number → Foreground
            #FFFF8218
        */
        static int _identifier = 123456789;
        /* UserTypesDelegates
            Text Editor →  User Types - Delegates → Foreground
            #FF030772
        */

        delegate void UserTypesDelegates();

        /* ///<Xml_Doc_Comments_Name>
            Text Editor →  XML Doc Comments - Name → Foreground
            #FF4D4D4D
        */
        /* XML Doc Comment
            Text Editor →  XML Doc Comment - Text → Foreground
            #FF6B6B6B
        */

        /// <Xml_Doc_Comments_Name>
        /// XML Doc Comment - Text
        /// The quick brown fox jumps over the lazy dog
        /// THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
        /// </Xml_Doc_Comments_Name>
        public static void IdentifierMethod(string[] args)
        {
            /* "The time now is approximately " 
                Text Editor →  String → Foreground
                #FF890303
            */
            string identifierString = "The time now is approximately " + DateTime.Now;
            /* http://packmyboxwith/jugs.html?q=five-dozen&t=liquor
                Text Editor → URL Hyperlink → Foreground
                #FF0015FF
            */
            Uri identifierUri = new Uri("http://packmyboxwith/jugs.html?q=five-dozen&t=liquor");
            /* @""
                Text Editor → String - Verbatim → Foreground
                #FF890303
            */
            Regex identifierRegex = new Regex(@"\S#$", RegexOptions.IgnorePatternWhitespace);

            for (int O = 0; O < 123456789; O++)
            {
                _identifier += (O%3)*((O/1) ^ 2) - 5;
                if (!identifierRegex.IsMatch(identifierUri.ToString()))
                {
                    /* Comment
                        Text Editor →  Comment → Foreground
                        #FF9F9F9F
                    */
                    // Comment
                    Console.WriteLine(identifierUri + identifierString);
                    /* MyEnum
                        Text Editor → User Types - Enums → Foreground
                        #FF00C3C3
                    */
                    var myEnum = MyEnum.ValueA;
                    /* MyStruct
                        Text Editor → User Types - Structures → Foreground 
                        #FF00C3C3
                    */
                    var myStruct = MyStruct.ValueA;
                }
            }
        }
    }

    /* #endregion
        Text Editor → Preprocessor Keyword → Foreground
        #FFFF00EA
    */

    #endregion

    public class Highlighting
    {
        /* >>> Selected Text
            Text Editor → Selected Text → Background
            #FF3399FF
        */
        /* >>> Inactive Selected Text
            Text Editor → Inactive Selected Text → Background
            #FFC0CEDC
        */
        string inactiveSelection = "select this";
        /* >>> Highlighted Definition
            Text Editor → Highlighted Definition → Background
            #FFDBE0CC
        */
        string highlightedRef;
        /* >>> Collapsed Text (REQUIRES VS RESTART)
            Text Editor → Collapsed Text (Collapsed) → Foreground
            #FF00FFFF
        */
        /* Collapsed Text
        */

        /* >>> Highlighted Reference
            Text Editor → Highlighted Reference → Background
            #FFDBE0CC
        */
        string selectString = "";

        public void Features()
        {
            /* >>> Highlighted Written Reference
                Text Editor → Highlighted Written Reference → Background
                #FFDBE0CC
            */
            highlightedRef = "again";
            /* >>> Breakpoint 
                Text Editor → Breakpoint (Enabled) → Background
                #FF963A46
            */
            var breakPoint = "breakpoint";
            /* PREPROCESSOR
                Text Editor → Identifier → Foreground
                #FF953BA3
            */
#if PREPROCESSOR
    /* excludedCode
        Text Editor → Excluded Code → Foreground
//      #FF808080
    */
    object excludedCode = null;
#endif
            /* current statement
                Text Editor → Current Statement → Background
                #FFFFEE62
            */
            breakPoint = "current statement";
        }
    }

    /* T
        Text Editor → User Types - Type Parameters → Foreground
        #FFFF8218
    */

    public class MyClass<T> where T : class
    {
    }

    /* MyEnum
        Text Editor → User Types - Enums → Foreground
        #FF00C3C3
    */

    public enum MyEnum
    {
        ValueA
    }

    /* MyStruct
        Text Editor → User Types - Structures → Foreground 
        #FF00C3C3
    */

    public struct MyStruct
    {
        public static object ValueA;
    }

    /* IUserTypesInterfaces
        Text Editor → User Types - Interfaces → Foreground
        #FF030772
    */

    internal interface IUserTypesInterfaces
    {
    }
}