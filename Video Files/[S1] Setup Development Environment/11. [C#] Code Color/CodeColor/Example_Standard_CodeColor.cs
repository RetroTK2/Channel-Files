﻿#region Plain Text

using System;
using System.Text.RegularExpressions;

namespace ExampleStandardCodeColor
{
    class UserTypesClasses : IUserTypesInterfaces
    {
        static int _identifier = 123456789;

        delegate void UserTypesDelegates();

        /// <Xml_Doc_Comments_Name>
        /// XML Doc Comment - Text
        /// The quick brown fox jumps over the lazy dog
        /// THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
        /// </Xml_Doc_Comments_Name>
        public static void IdentifierMethod(string[] args)
        {
            string identifierString = "The time now is approximately " + DateTime.Now;
            Uri identifierUri = new Uri("http://packmyboxwith/jugs.html?q=five-dozen&t=liquor");
            Regex identifierRegex = new Regex(@"\S#$", RegexOptions.IgnorePatternWhitespace);

            for (int O = 0; O < 123456789; O++)
            {
                _identifier += (O%3)*((O/1) ^ 2) - 5;
                if (!identifierRegex.IsMatch(identifierUri.ToString()))
                {
                    // Comment
                    Console.WriteLine(identifierUri + identifierString);
                    var myEnum = MyEnum.ValueA;
                    var myStruct = MyStruct.ValueA;
                }
            }
        }
    }

    #endregion

    public class Highlighting
    {
        string inactiveSelection = "select this";
        string highlightedRef;
        /* Collapsed Text
        */
        string selectString = "";

        public void Features()
        {
            highlightedRef = "again";
            var breakPoint = "breakpoint";
#if PREPROCESSOR
    object excludedCode = null;
#endif
            breakPoint = "current statement";
        }
    }

    public class MyClass<T> where T : class
    {
    }

    public enum MyEnum
    {
        ValueA
    }

    public struct MyStruct
    {
        public static object ValueA;
    }

    internal interface IUserTypesInterfaces
    {
    }
}