﻿/* #region
    Text Editor → Preprocessor Keyword → Foreground
    #FFFF00EA
*/
/* Plain Text
    Text Editor → Plain Text → Foreground
    #FF000000
*/

#region Plain Text

/* using
    Text Editor → Keyword → Foreground
    #FF007AFF
*/
/* System
    Text Editor → ReSharper Namespace identifier → Foreground
    #FF007911
*/
/* ;
    Text Editor → Plain Text → Foreground
    #FF000000
*/
using System;
using System.Text.RegularExpressions;

/* class
    Text Editor → Keyword → Foreground
    #FF007AFF
*/
/* ResharperCodeColor
    Text Editor → ReSharper Namespace identifier → Foreground
    #FF007911
*/

namespace ResharperCodeColor
{
    /* UserTypesClasses
        Text Editor → ReSharper Class Identifier → Foreground
        #FF53D327
    */
    /* MyOtherNamespace
        Text Editor → ReSharper Namespace identifier → Foreground
        #FF007911
    */
    /* IUserTypesInterfaces
        Text Editor → ReSharper Interface Identifier → Foreground
        #FF030772
    */

    class UserTypesClasses : MyOtherNamespace.IUserTypesInterfaces
    {
        /* CONSTANT_IDENTIFIER
            Text Editor → ReSharper Constant Identifier → Foreground
            #FFFF8218
        */
        public const string CONSTANT_IDENTIFIER = "";
        /* _fieldIdentifier
            Text Editor → ReSharper Field Identifier → Foreground
            #FF953BA3
        */
        /* 123456789
            Text Editor → Number → Foreground
            #FFFF8218
        */
        static int _fieldIdentifier = 123456789;
        /* UserTypesDelegates
            Text Editor →  Resharper Delegate Identifier → Foreground
            #FF030772
        */

        public delegate void UserTypesDelegates();

        /* ///<Xml_Doc_Comments_Name>
            Text Editor →  XML Doc Comments - Name → Foreground
            #FF4D4D4D
        */
        /* XML Doc Comment
            Text Editor →  XML Doc Comment - Text → Foreground
            #FF6B6B6B
        */

        /// <Xml_Doc_Comments_Name>
        /// XML Doc Comment - Text
        /// The quick brown fox jumps over the lazy dog
        /// THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG
        /// </Xml_Doc_Comments_Name>
        /* MethodIdentifier
            Text Editor →  ReSharper Method Identifier → Foreground
            #FF008B8B
        */
        /* args
            Text Editor →  ReSharper Parameter Identifier → Foreground
            Text Editor → Identifier → Foreground (If ReSharper Parameter Identifier is set to Default)
            #FF953BA3
        */
        public static void MethodIdentifier(string[] args)
        {
            /* identifierString
                Text Editor → Identifier → Foreground
                #FF953BA3
            */
            /* "The time now is approximately " 
                Text Editor →  String → Foreground
                #FF890303
            */
            /* DateTime
                Text Editor → ReSharper Struct Identifier → Foreground
                #FF00C3C3
            */
            /* Now
                Text Editor → ReSharper Field Identifier → Foreground
                #FF953BA3
            */
            string identifierString = "The time now is approximately " + DateTime.Now;
            /* http://packmyboxwith/jugs.html?q=five-dozen&t=liquor
                Text Editor → URL Hyperlink → Foreground
                #FF0015FF
            */
            Uri identifierUri = new Uri("http://packmyboxwith/jugs.html?q=five-dozen&t=liquor");
            /* @""
                Text Editor → String - Verbatim → Foreground
                #FF890303
            */
            /* \S
                Text Editor → ReSharper Regex Identifier → Foreground 
                #FF00A5F4
            */
            /* #$
                Text Editor → ReSharper Regex Comment → Foreground 
                #FF008000
            */
            Regex identifierRegex = new Regex(@"\S#$", RegexOptions.IgnorePatternWhitespace);
            for (int O = 0; O < 123456789; O++)
            {
                _fieldIdentifier += (O%3)*((O/1) ^ 2) - 5;
                if (!identifierRegex.IsMatch(identifierUri.ToString()))
                {
                    /* Comment
                        Text Editor →  Comment → Foreground
                        #FF9F9F9F
                    */
                    // Comment
                    Console.WriteLine(identifierUri + identifierString + _fieldIdentifier);
                    /* MyEnum
                        Text Editor → ReSharper Enum Identifier → Foreground
                        #FF00C3C3
                    */
                    /* ValueA
                        Text Editor → ReSharper Constant Identifier → Foreground
                        #FFFF8218
                    */
                    var myEnum = MyEnum.ValueA;
                    /* MyStruct
                        Text Editor → ReSharper Struct Identifier → Foreground
                        #FF00C3C3
                    */
                    /* ValueA
                        Text Editor → ReSharper Field Identifier → Foreground
                        #FF953BA3
                    */
                    /* PropertyA
                        Text Editor → ReSharper Field Identifier → Foreground
                        #FF953BA3
                    */
                    MyStruct.ValueA = 10;
                    var myStruct = new MyStruct();
                    myStruct.PropertyA = 10;
                }
            }

            var myExtendedClass = new MyExtendedClass();
            /* ExtensionMethod
                Text Editor → ReSharper Extension Method Identifier → Foreground 
                #FF797A00
            */
            myExtendedClass.ExtensionMethod();
        }
    }

    /* #endregion
        Text Editor → Preprocessor Keyword → Foreground
        #FFFF00EA
    */

    #endregion

    public class Highlighting
    {
        /* >>> Selected Text
            Text Editor → Selected Text → Background
            #FF3399FF
        */
        /* >>> Inactive Selected Text
            Text Editor → Inactive Selected Text → Background
            #FFC0CEDC
        */
        string inactiveSelection = "select this";
        /* >>> Highlighted Definition
            Text Editor → Highlighted Definition → Background
            #FFDBE0CC
        */
        string highlightedRef;
        /* >>> Collapsed Text (REQUIRES VS RESTART)
            Text Editor → Collapsed Text (Collapsed) → Foreground
            #FF00FFFF
        */
        /* Collapsed Text
        */

        /* >>> Highlighted Reference
            Text Editor → Highlighted Reference → Background
            #FFDBE0CC
        */
        string selectString = "";

        public void Features()
        {
            /* >>> Highlighted Written Reference
                Text Editor → Highlighted Written Reference → Background
                #FFDBE0CC
            */
            highlightedRef = "again";
            /* >>> Breakpoint 
                Text Editor → Breakpoint (Enabled) → Background
                #FF963A46
            */
            var breakPoint = "breakpoint";
            /* PREPROCESSOR
                Text Editor → Identifier → Foreground
                #FF953BA3
            */
#if PREPROCESSOR
    /* excludedCode
        Text Editor → Excluded Code → Foreground
//      #FF808080
    */
        object excludedCode = null;
#endif
            /* current statement
                Text Editor → Current Statement → Background
                #FFFFEE62
            */
            breakPoint = "current statement";
        }
    }

    /* T
        Text Editor → ReSharper Type Parameter Identifier → Foreground
        #FFFF8218
    */

    public class MyClass<T> where T : class
    {
    }

    /* MyEnum
        Text Editor → ReSharper Enum Identifier → Foreground
        #FF00C3C3
    */
    /* ValueA
        Text Editor → ReSharper Constant Identifier → Foreground
        #FFFF8218
    */

    public enum MyEnum
    {
        ValueA
    }

    /* MyStruct
        Text Editor → ReSharper Struct Identifier → Foreground
        #FF00C3C3
    */
    /* PropertyA
        Text Editor → ReSharper Field Identifier → Foreground
        #FF953BA3
    */

    public struct MyStruct
    {
        public static object ValueA;

        public int PropertyA { get; set; }
    }

    /* IUserTypesInterfaces
        Text Editor → ReSharper Interface Identifier → Foreground
        #FF030772
    */

    namespace MyOtherNamespace
    {
        internal interface IUserTypesInterfaces
        {
        }
    }

    /* MyStaticClass
        Text Editor → ReSharper Static Class Identifier → Foreground
        #FF53D327
    */

    public static class MyStaticClass
    {
        public static void ExtensionMethod(this MyExtendedClass theExtendedClass)
        {
        }
    }

    public class MyExtendedClass
    {
    }

    /* >>> ReSharper Current Line
        Text Editor → ReSharper Current Line Highlight → Background
        #FFFFFFE0
    */

    public class DeadCode
    {
        /* DeadMethod (Dead/Unused Code In General)
            Text Editor → ReSharper Dead Code → Foreground
            #FF000000
        */

        void DeadMethod()
        {
        }
    }
}