﻿using System.Collections.Generic;

namespace ReSharperDemo
{
    internal class Program
    {
        private const string MY_NEW_CONSTANT = "";

        private List<int> _myList = new List<int>();

        private static void Main(string[] args)
        {
            var theNewestClass = new MyNewestClass();
            theNewestClass.Run();
        }
    }
}