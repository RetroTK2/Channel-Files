﻿using UnityEngine;
using System.Collections;

public class MyMonoBehaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Debug.Log ("Log Message");
		Debug.LogWarning ("Warning");
		Debug.LogError ("Error");
	}

	// Update is called once per frame
	void Update () {
		Debug.Log ("Update");
	}
}
