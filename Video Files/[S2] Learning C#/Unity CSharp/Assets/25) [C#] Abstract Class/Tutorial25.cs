﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tutorial25
{
    public class Tutorial25 : MonoBehaviour
    {
        void Start()
        {
            var shape = new RightAngledTriangle(3,4,5);
        }
    }

    public class RightAngledTriangle : Triangle
    {
        private readonly int _length;
        private readonly int _breadth;
        private readonly int _hypotenuses;

        public RightAngledTriangle(int length, int breadth, int hypotenuses)
        {
            _length = length;
            _breadth = breadth;
            _hypotenuses = hypotenuses;
        }

        public override float Area
        {
            get { return (_length * _breadth) / 2f; }
        }

        public override float Perimeter
        {
            get { return _length + _breadth + _hypotenuses; }
        }
    }

    public abstract class Shape
    {
        public abstract float Area { get; }
        public abstract float Perimeter { get; }

        public void LogPerimeter()
        {
            logValue("Perimeter", Perimeter);
        }

        public void LogArea()
        {
            logValue("Area", Area);
        }

        private void logValue(string valueName, float theValue)
        {
            "{1} >> {2}: {0}".Log(theValue.ToString(), ToString(), valueName);
        }
    }

    public class Rectangle : Shape
    {
        private readonly float _width;
        private readonly float _breadth;

        public Rectangle(float width, float breadth)
        {
            _width = width;
            _breadth = breadth;
        }

        public override float Area
        {
            get { return _width * _breadth; }
        }

        public override float Perimeter
        {
            get { return (2 * _width) + (2 * _breadth); }
        }

        public override string ToString()
        {
            return "Rectangle";
        }
    }

    public class Square : Rectangle
    {
        public Square(float length) : base(length, length)
        {
        }

        public override string ToString()
        {
            return "Square";
        }
    }

    public class Circle : Shape
    {
        private readonly float _radius;

        public Circle(float radius)
        {
            _radius = radius;
        }

        public override float Area
        {
            get
            {
                return Mathf.PI * Mathf.Pow(_radius, 2);
            }
        }

        public override float Perimeter
        {
            get { return Mathf.PI * (_radius * 2); }
        }
        public override string ToString()
        {
            return "Circle";
        }
    }

    public abstract class Triangle : Shape
    {
    }

    public class EquilateralTriangle : Shape
    {
        private readonly float _length;

        public EquilateralTriangle(float length)
        {
            _length = length;
        }

        public override float Perimeter
        {
            get { return _length * 3; }
        }

        public override float Area
        {
            get { return (Mathf.Sqrt(3) / 4f) * Mathf.Pow(_length, 2); }
        }

        public override string ToString()
        {
            return "Equilateral Triangle";
        }
    }

}