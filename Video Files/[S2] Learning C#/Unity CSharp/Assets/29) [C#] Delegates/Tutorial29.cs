﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial29 : MonoBehaviour
{
    private static Del1 Handler1;
    public delegate void Del1(string str);

    public delegate int Del2(int i);

    void action1(string str)
    {
        str.Log();
    }

    void action2(string str)
    {
        "{0}{1}".Log("action 2 - ", str);
    }

    int actionInt1(int i)
    {
        "10 - {0}".Log(i);
        return 10;
    }

    int actionInt2(int i)
    {
        "20 - {0}".Log(i);

        return 20;
    }


    int actionInt3(int i)
    {
        "30 - {0}".Log(i);
        return 30;
    }

    void logAction(string s)
    {
        transform.name.Log();
    }

    void Awake()
    {
        Handler1 += logAction;
    }

    void Start ()
    {
        if (transform.name == "Main Camera")Handler1("");
//	    Del1 handler = action1;
//	    handler += action2;
//	    handler = action2 + handler;
//	    handler("A");

        //	    Del2 handler2 = actionInt1;
        //	    handler2 += actionInt3;
        //	    handler2 += actionInt2;

        //	    handler2(0).ToString().Log();

        //        handler2 -= actionInt3;

        //	    foreach (var action in handler2.GetInvocationList())
        //	    {
        //	        action.DynamicInvoke(10);
        //	    }
    }
}
