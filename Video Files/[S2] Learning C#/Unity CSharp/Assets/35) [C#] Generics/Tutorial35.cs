﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial35 : MonoBehaviour {

	void Start () {
	    List<int> _list = new List<int>();
	    Action<string> action = (x) => { x += "a"; };
	    Action<int> action2 = (x) => { x += 1; };

        var gc = new GenericClass<MyClass35>(new MyClass35());
	    gc.Log();
	    var lol = gc.GetObject();
	}
}

public class MyClass35
{
    public MyClass35()
    {

    }
    public MyClass35(int x)
    {
            
    }
}

public class GenericClass<T> : IInt35<T,T> where T : class, new() //where U : class where V : MyClass35
{
    private readonly T _myObj;

    public GenericClass(T  myObj )
    {
        _myObj = myObj;
    }

    public object GetObject()
    {
        return _myObj;
    }

    public void Log()
    {
        _myObj.ToString().Log();
    }
}


public interface IInt35<TInput,TOutput>
{

}