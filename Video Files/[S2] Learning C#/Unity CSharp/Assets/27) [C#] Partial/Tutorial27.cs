﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial27 : MonoBehaviour {
	void Start () {
		var obj = new PartialClass();
        obj.Method1();
        obj.Method2();
        obj.Log();
	    var obj2 = new PartialClass.PartialClassNested();
	    obj2.method1();
    }
}

public partial class PartialClass
{
    partial void methodA();
    public void Log()
    {
        methodA();
    }
    public partial class PartialClassNested : ILol
    {
        public void method2()
        {
            
        }


        public void pop()
        {
            
        }
    }

    public void Method1()
    {
    }
}

public partial class PartialClass
{
    partial void methodA()
    {
        "log method A".Log();
    }

    public partial class PartialClassNested : IPartial
    {
        public void method1() { }
        public void method()
        {
        }
    }

    public void Method2()
    {
    }
}

public interface ILol
{
    void pop();
}

public partial interface IPartial
{
    void method();

}

public partial interface IPartial
{
    void method2();
}