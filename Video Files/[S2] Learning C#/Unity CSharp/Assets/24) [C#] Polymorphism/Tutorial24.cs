﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Tutorial24
{ 
public class Tutorial24 : MonoBehaviour {
	void Start ()
	{
        Shape[] shapes = new Shape[] { new Circle(5), new Rectangle(3, 4), new EquilateralTriangle(4) };
	    foreach (var shape in shapes)
	    {
	        shape.LogPerimeter();
	        shape.LogArea();
	    }
	}

    void l()
    {
        Shape shape = new Circle(5);
        shape.Area.ToString().Log();
    }
}

public class Shape
{
    public virtual float Area
    {
        get { return 0; }
    }
    public virtual float Perimeter { get { return 0; } }

    public void LogPerimeter()
    {
        logValue("Perimeter", Perimeter);
    }

    public void LogArea()
    {
        logValue("Area", Area);
    }

    private void logValue(string valueName, float theValue)
    {
        "{1} >> {2}: {0}".Log(theValue.ToString(), ToString(), valueName);
    }
}

public class Rectangle : Shape
{
    private readonly float _width;
    private readonly float _breadth;

    public Rectangle(float width, float breadth)
    {
        _width = width;
        _breadth = breadth;
    }

    public override float Area
    {
        get { return _width * _breadth; }
    }

    public override float Perimeter
    {
        get { return (2 * _width) + (2 * _breadth); }
    }

    public override string ToString()
    {
        return "Rectangle";
    }
}

public class Square : Rectangle
{
    public Square(float length) : base(length, length)
    {
    }

    public override string ToString()
    {
        return "Square";
    }
}

public class Circle : Shape
{
    private readonly float _radius;

    public Circle(float radius)
    {
        _radius = radius;
    }

    public override float Area
    {
        get
        {
            return Mathf.PI * Mathf.Pow(_radius,2);
        }
    }

    public override float Perimeter
    {
        get { return Mathf.PI * (_radius * 2); }
    }
    public override string ToString()
    {
        return "Circle";
    }
}

public class Triangle : Shape
{
}

public class L : EquilateralTriangle
{
    public L(float length) : base(length)
    {
    }
}


public class EquilateralTriangle : Shape
{
    private readonly float _length;

    public EquilateralTriangle(float length)
    {
        _length = length;
    }

    public override float Perimeter
    {
        get { return _length * 3; }
    }

    public new float Area
    {
        get { return (Mathf.Sqrt(3) / 4f) * Mathf.Pow(_length,2); }
    }

    public override string ToString()
    {
        return "Equilateral Triangle";
    }
}
}