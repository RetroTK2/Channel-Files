﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial28 : MonoBehaviour
{
    void Start()
    {

    }
}

public class ReadonlyExample
{
    readonly int _myInt = 100;

    public ReadonlyExample()
    {
        _myInt = 10;
    }

   public void Method()
    {
        _myInt.ToString().Log();
    }
}