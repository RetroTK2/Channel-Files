﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial30 : MonoBehaviour
{
    public delegate int MethodDel(string s);

    MethodDel _methodDelEvent;

    public event MethodDel MethodDelEvent
    {
        add
        {
            "add".Log();
            _methodDelEvent += value;
        }
        remove
        {
            "remove".Log();
            _methodDelEvent -= value;
        }
    }
    public void Reset()
    {
        _methodDelEvent = null;
    }
  public  void Invoke(string s)
    {
        _methodDelEvent(s);
    }

    void Start ()
    {
        MethodDelEvent += method;
        MethodDelEvent -= method;
        Invoke("s");
    }

    int method(string s)
    {
        s.Log();
        return 0;
    }
}
