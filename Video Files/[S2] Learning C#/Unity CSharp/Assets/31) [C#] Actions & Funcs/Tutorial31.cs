﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial31 : MonoBehaviour
{
    private Action action;
    private Action<int> actionInt;
    private Func<string> funcString;
    private Func<int,int,string> funcString2;

    void Start ()
    {
        action = method;
        actionInt = method2;
        funcString = method3;
        funcString2 = method4;
        action();
        actionInt(99);
        funcString().Log();
        funcString2(9, 1).Log();
    }

    private string method4(int x, int y)
    {
        return x + " " + y;
    }

    void method()
    {
        "log".Log();
    }

    void method2(int i)
    {
        i.ToString().Log();
    }

    string method3()
    {
        return "funcString";
    }
}
