﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial33 : MonoBehaviour
{
    public static int MyInt;

    void Start ()
    {
        Stat33.prop = 0;

        MyInt = 0;

        A33.MyStr = "string";
        A33.MyStr.Log();

        B33.MyStr = "myStr";
        B33.MyStr.Log();
        A33.MyStr.Log();
    }

    void Update()
    {
        A33.MyStr.Log();
    }
}

public class B33 : A33
{
}

public class A33
{
    public static int prop
    {
        get { return 0; }
    }


    public static string MyStr;

    void method()
    {

    }
}

public static class Stat33
{
    static Stat33()
    {
        A33.MyStr = "hello";
        A33.MyStr.Log();
    }

    public static int prop { get; set; }

   public static void method()
   {
       "static method".Log();
   }
}