﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial32 : MonoBehaviour {

    void Start ()
    {
        var i = 10;
        Action action = () => { };
        Action<int> actionInt = myInt =>
        {
            i.ToString().Log();
            myInt.ToString().Log();
            myInt.ToString().Log();
        };

        action();
        actionInt(99);

        Func<string, string> func = (x) => { return x;};

        var str = func("new func");
        str.Log();

        for (int x = 0; x < 10; x++)
        {
           
            func = (y) =>
            {
                for (int z = 0; x < 10; x++)
                {
                    continue;
                }

                x.ToString().Log();

                return "";
            };
        }

        func("");

        Action<Action<int>> act = (x) => { x(11); };
        act((y) => y.ToString().Log());

        Action<int> act2 = (x) => { };

        act2 = delegate(int p)
        {
            p.ToString().Log();
            p .ToString().Log();
        };
        act2 = (p) =>
        {
            p.ToString().Log();
            p.ToString().Log();
        };

    }

}
