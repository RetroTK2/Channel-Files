﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial26 : MonoBehaviour {
    void Start ()
    {
        method(new Implementation());
        method(new Implementation2());
    }

    void method(IAnother inter)
    {
        inter.Log();
    }
}

class Implementation : IInterface, IAnother
{
    public void method()
    {
    }

    public int IntProp { get; set; }

    public void Public() { }
    public void Log()
    {
        "Implementation".Log();
    }
}

class SharedBehaviour
{
    
}

class Implementation2 : Implementation, IInterface, IAnother
{
    public  void method()
    {
    }

    public int IntProp { get; set; }
    public void Log()
    {
        "Implementation2".Log();
    }
}

public interface IInterface 
{
    void method();
    int IntProp { get; set; }
}

public interface IAnother
{
    void Log();
}

